<?php

/*
 * The 2-Clause BSD License
 *
 * Copyright (c) 2019-2020, Eirwego
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

declare(strict_types=1);

namespace Eirwego\License;

use Eirwego\License;
use Exception;
use JMS\Serializer\SerializerBuilder;

class Reader
{
    /**
     * @var string
     */
    private $cipherMethod;

    /**
     * @var string
     */
    private $content;

    /**
     * @var bool
     */
    private $locked;

    /**
     * @var string
     */
    private $signature;

    /**
     * @var bool
     */
    private $verified;

    public function __construct(string $basename)
    {
        $content = file_get_contents($basename.'.lic');

        if (false === $content) {
            throw new Exception('Unable to read license at '.$basename.'.lic');
        }

        $signature = file_get_contents($basename.'.sig');

        if (false === $signature) {
            throw new Exception('Unable to read signature at '.$basename.'.sig');
        }

        $this->content = $content;
        $this->locked = true;
        $this->signature = $signature;
        $this->verified = false;

        $this->setCipherMethod('aes-128-cbc');
    }

    public function getLicense(): License
    {
        if ($this->locked) {
            throw new Exception('You have to unlock the license');
        }

        $serializer = SerializerBuilder::create()->addMetadataDir(__DIR__.'/../config/serializer')->build();
        $license = $serializer->deserialize($this->content, License::class, 'json');

        if (!$license instanceof License) {
            throw new Exception('Unable to extract license');
        }

        return $license;
    }

    private function setCipherMethod(string $cipherMethod)
    {
        if (!in_array($cipherMethod, openssl_get_cipher_methods())) {
            throw new Exception($cipherMethod.' cipher method not available');
        }

        $this->cipherMethod = $cipherMethod;
    }

    public function unlock(string $key, string $iv)
    {
        if (!$this->locked) {
            return;
        }

        if (!$this->verified) {
            throw new Exception('You have to verify the license first');
        }

        $data = openssl_decrypt($this->content, $this->cipherMethod, $key, 0, hex2bin($iv) ?: '');

        if (false === $data) {
            throw new Exception('Unable to unlock');
        }

        $content = base64_decode($data);

        if (false === $content) {
            throw new Exception('Unable decode data');
        }

        $this->content = $content;
        $this->locked = false;
    }

    public function verify(string $publicKey)
    {
        $publicKeyResource = openssl_pkey_get_public($publicKey);

        if (false === $publicKeyResource) {
            throw new Exception('Unable to open public key');
        }

        if (1 !== openssl_verify($this->content, $this->signature, $publicKeyResource)) {
            $this->verified = false;
            throw new Exception('Signature is incorrect');
        }

        $this->verified = true;
    }
}
